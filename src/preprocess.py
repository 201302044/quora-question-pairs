import csv
import nltk
from nltk.tokenize import RegexpTokenizer
import random
import sys
import pickle
from keras.preprocessing import sequence, text
import pandas as pd
from pattern.text.en import singularize

tokenizer = RegexpTokenizer(r'\w+')

data = pd.read_csv('../train.csv', sep=',')
y = data.is_duplicate.values

tk = text.Tokenizer(num_words=300000)

max_len = 44
tk.fit_on_texts(list(data.question1.values) + list(data.question2.values.astype(str)))
x1 = tk.texts_to_sequences(data.question1.values)
x1 = sequence.pad_sequences(x1, maxlen=max_len)

x2 = tk.texts_to_sequences(data.question2.values.astype(str))
x2 = sequence.pad_sequences(x2, maxlen=max_len)

word_index = tk.word_index

file = open("../train.csv")
L = len(file.readlines())

trigramHashes = set()

def parse(sent):
	for c in sent:
		trigramHashes.add(c)
a = []

from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_extraction.text import CountVectorizer
tfidf = TfidfVectorizer(lowercase=False, )

questions = []
b = []

with open('../train.csv') as f:
	l = 0
	data = csv.reader(f, delimiter = ',')
	for row in data:
		assert(len(row) == 6)
		q1 = row[3].lower()
		q1 = tokenizer.tokenize(q1)
		q2 = row[4].lower()
		q2 = tokenizer.tokenize(q2)

		q11 = ' '.join(q1)
		q21 = ' '.join(q2)

		parse(q11)
		parse(q21)

		while(len(q11) > 300):
			q11 = q11[:-1]
		while(len(q11) < 300):
			q11 = ' ' + q11

		while(len(q21) > 300):
			q21 = q21[:-1]
		while(len(q21) < 300):
			q21 = ' ' + q21
		
		l += 1
		if(l % 100 == 0):
			sys.stdout.write("%d/%d\r      " % (l, L))
		try:
			sim = int(row[5])
		except Exception as e:
			print e
			print row
			continue
		while(len(q1) > max_len):
			q1.pop()
		while(len(q2) > max_len):
			q2.pop()
		while(len(q1) < max_len):
			q1 = ['#'] + q1
		while(len(q2) < max_len):
			q2 = ['#'] + q2
		a.append([q1, q2, sim])
		b.append([q11, q21, sim])

#tfidf.fit_transform(questions)
# dict key:word and value:tf-idf score
#word2tfidf = dict(zip(tfidf.get_feature_names(), tfidf.idf_))

print
L = len(a)
testSetLen = int(15 * L / 100.0) #15%
cur = 0
pres = set()
while(cur < testSetLen):
	x = random.randint(0, L - 2)
	if(x not in pres):
		cur += 1
		pres.add(x)

train = []
test = []
least = 10000000
most = 0

for i in xrange(L):
	least = min(least, len(a[i][0]))
	least = min(least, len(a[i][1]))

	most = max(most, len(a[i][0]))
	most = max(most, len(a[i][1]))

print least, most
l = 0

file = open("../test.csv")
L = len(file.readlines())

with open('../test.csv') as f:
	l = 0
	data = csv.reader(f, delimiter = ',')
	for row in data:
		assert(len(row) == 3)
		q1 = row[1].lower()
		q1 = tokenizer.tokenize(q1)
		q2 = row[2].lower()
		q2 = tokenizer.tokenize(q2)
		
		q1 = ' '.join(q1)
		q2 = ' '.join(q2)

		parse(q1)
		parse(q2)

		l += 1
		if(l % 100 == 0):
			sys.stdout.write("%d/%d\r      " % (l, L))

print

print len(trigramHashes)

f = open('train.pkl', 'wb')
pickle.dump(a, f)

f = open('train_character_CNN.pkl', 'wb')
pickle.dump(b, f)

f = open('trigramHashes.pkl', 'wb')
pickle.dump(trigramHashes, f)

f = open('train_q1.pkl', 'wb')
pickle.dump(x1, f)

f = open('train_q2.pkl', 'wb')
pickle.dump(x2, f)

f = open('label.pkl', 'wb')
pickle.dump(y, f)

f = open('wordindex.pkl', 'wb')
pickle.dump(word_index, f)

"""
f = open('word2tfidf.pkl', 'wb')
pickle.dump(word2tfidf, f)
"""