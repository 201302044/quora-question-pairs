#!/usr/bin/python

import numpy as np
import os
import sys
import tensorflow as tf
from keras.datasets import mnist
from keras.layers.core import Dense, Dropout, Activation
from keras.layers.embeddings import Embedding
from keras.layers import LSTM
from keras.layers import Input, merge, MaxPooling2D
from keras.models import Sequential, Model, load_model
from keras.layers import Dense, Dropout, Input, Lambda, Merge, Flatten, Convolution2D, Convolution1D
from keras.optimizers import SGD, RMSprop, Adam, Nadam, Adadelta
from keras.callbacks import *
from keras.utils import np_utils
from keras import backend as K
from keras import callbacks
import random
import pickle
from gensim.models import Word2Vec
import math
from tqdm import tqdm
from keras.utils import plot_model
from pattern.text.en import singularize

print 'Loading Data'

f = open('train.pkl')
data = pickle.load(f)

f = open('train_character_CNN.pkl')
data1 = pickle.load(f)

f = open('trigramHashes.pkl')
trigramHashes = pickle.load(f)

index2word = {}
hash2index = {}

j = 0
for i in trigramHashes:
   hash2index[i] = j
   j += 1

GloveWord_to_vec = {}
f = open('../data/word2vec.googlenews.300.txt')
#f = open('../is_that_a_duplicate_quora_question/data/glove.840B.300d.txt')
next(f)
for line in tqdm(f):
    values = line.split()
    word = values[0]
    assert(len(values[1:]) == 300)
    coefs = np.asarray(values[1:], dtype='float32')
    GloveWord_to_vec[word] = coefs
f.close()

#print GloveWord_to_vec["analyze"]
#print np.zeros(300)
#exit(0)

MAX_SEQ_LEN = 44
MAX_CHAR_SEQ_LEN = 300
TRIGRAM_HASHES_LEN = len(trigramHashes)
EPOCH = 40
BATCH_SIZE = 512
DATA_LEN = len(data)
TSET_LEN = 256 * 100

trainData = {}
testSet = set()
tLen = 0

while(tLen < TSET_LEN):
   idx = random.randint(0, DATA_LEN - 2)
   if(idx not in testSet):
      testSet.add(idx)
      tLen += 1

print tLen

for i in xrange(DATA_LEN):
   trainData[i] = True

for i in testSet:
   trainData[i] = False

def network():
   model1 = Sequential()
   model1.add(Convolution2D(5, (4, TRIGRAM_HASHES_LEN), border_mode = 'same', input_shape = (MAX_CHAR_SEQ_LEN, TRIGRAM_HASHES_LEN, 1, ), activation = 'tanh'))
   #model1.add(Convolution2D(5, (4, TRIGRAM_HASHES_LEN), border_mode = 'same', activation = 'tanh'))
   #model1.add(MaxPooling2D(pool_size = (3, TRIGRAM_HASHES_LEN)))
   model1.add(Flatten())
   model1.add(Dense(128, activation ='tanh'))
   model1.add(Dropout(0.2))
   model1.add(Dense(128, activation ='tanh'))
   model1.add(Dropout(0.2))
   model1.add(Dense(128, activation ='tanh'))
   model1.add(Dropout(0.2))

   model2 = Sequential()
   model2.add(LSTM(128, return_sequences = False, input_shape = (MAX_SEQ_LEN, 300, )))
   model2.add(Dense(128, activation = 'tanh'))
   model2.add(Dropout(0.2))
   model2.add(Dense(128, activation = 'tanh'))
   model2.add(Dropout(0.2))
   model2.add(Dense(128, activation = 'tanh'))
   model2.add(Dropout(0.2))

   model3 = Sequential()
   model3.add(Merge([model1, model2], mode = 'concat'))
   model3.add(Dense(128, activation = 'tanh'))
   model3.add(Dropout(0.2))
   model3.add(Dense(128, activation = 'tanh'))
   model3.add(Dropout(0.2))
   model3.add(Dense(128, activation = 'tanh'))
   model3.add(Dropout(0.2))

   return model3

input11 = Input(shape = (MAX_CHAR_SEQ_LEN, TRIGRAM_HASHES_LEN, 1, ))
input12 = Input(shape = (MAX_SEQ_LEN, 300, ))

input21 = Input(shape = (MAX_CHAR_SEQ_LEN, TRIGRAM_HASHES_LEN, 1, ))
input22 = Input(shape = (MAX_SEQ_LEN, 300, ))


baseNetwork = network()

leftNetwork = baseNetwork([input11, input12])
rightNetwork = baseNetwork([input21, input22])

probability = Merge(mode = 'concat')([leftNetwork, rightNetwork])
probability = Dense(128, activation = 'tanh')(probability)
probability = Dropout(0.2)(probability)
probability = Dense(128, activation = 'tanh')(probability)
probability = Dropout(0.2)(probability)
probability = Dense(128, activation = 'tanh')(probability)
probability = Dense(2, activation = 'softmax')(probability)
probability = Dense(128, activation = 'tanh')(probability)
probability = Dense(2, activation = 'softmax')(probability)

model = Model(inputs = [input11, input12, input21, input22], outputs = [probability])

vecModel = Model(inputs = [input11, input12], outputs = leftNetwork)

plot_model(model, to_file='model.png', show_shapes = True)

#optimizer = SGD(lr = 0.1)
optimizer = Adadelta()
#optimizer = Adam()
#optimizer = RMSprop()

model.compile(loss = 'binary_crossentropy', optimizer = optimizer)

print TRIGRAM_HASHES_LEN

def makeHashVector(sent):
   res = []
   assert(len(sent) == MAX_CHAR_SEQ_LEN)
   res = np.zeros((MAX_CHAR_SEQ_LEN, TRIGRAM_HASHES_LEN))
   idx = 0
   for c in sent:
      res[idx][hash2index[c]] = 1
      idx += 1
   return res

def transformLabel(lab):
   if(lab == 0):
      return [1, 0]
   else:
      assert(lab == 1)
      return [0, 1]

def transformString(s):
   res = []
   for i in s:
      if(i != '#'):
         res.append(i)
   return ' '.join(res)

def getWordHashVectors(idx):
   res1 = []
   res2 = []
   res3 = []
   j = idx
   i = 0
   while(i < BATCH_SIZE):
      if(j >= DATA_LEN):
         break
      if(trainData[j]):
         sent1 = data1[j][0]
         sent2 = data1[j][1]
         _label = data1[j][2]
         res3.append(transformLabel(_label))
         res1.append(makeHashVector(sent1))
         res2.append(makeHashVector(sent2))
      i += 1
      j += 1
   #print
   #print TRIGRAM_HASHES_LEN
   #print len(res1)
   #print len(res1[0])
   #exit(0)
   res1 = np.asarray(res1)
   res2 = np.asarray(res2)

   res1 = res1.reshape(res1.shape[0], MAX_CHAR_SEQ_LEN, TRIGRAM_HASHES_LEN, 1)
   res2 = res2.reshape(res2.shape[0], MAX_CHAR_SEQ_LEN, TRIGRAM_HASHES_LEN, 1)
   return res1, res2, np.asarray(res3)

def makeGloveVector(sent):
   res = []
   assert(len(sent) == MAX_SEQ_LEN)
   for _word in sent:
   	  word = _word #singularize(_word)
   	  try:
   	  	res.append(GloveWord_to_vec[word])
   	  except KeyError:
   	  	res.append(np.zeros(300))
   return res

def getGloveVectors(idx):
   res1 = []
   res2 = []
   j = idx
   i = 0
   while(i < BATCH_SIZE):
      if(j >= DATA_LEN):
         break
      if(trainData[j]):
         sent1 = data[j][0]
         sent2 = data[j][1]
         _label = data[j][2]
         res1.append(makeGloveVector(sent1))
         res2.append(makeGloveVector(sent2))
      i += 1
      j += 1
   res1 = np.asarray(res1)
   res2 = np.asarray(res2)

   return res1, res2

def logloss(expected, predicted):
   assert(len(expected) == 2)
   assert(len(predicted) == 2)

   res = 0
   res += expected[0] * math.log(predicted[0], 10)
   res += expected[1] * math.log(predicted[1], 10)
   return -res

testSet = list(testSet)

for ep in xrange(1, EPOCH + 1):
   print "Epoch: %d" % ep
   i = 0
   err = 0
   total = 0
   res = 0
   while(i < DATA_LEN):
      v11 = None
      v12 = None
      v11, v21, y = getWordHashVectors(i)
      v12, v22 = getGloveVectors(i)
      try:
         cur = model.train_on_batch([v11, v12, v21, v22], y)
      except Exception:
         print
         print i
         print v11.shape
         print v12.shape
         print v21.shape
         print v22.shape
         exit(0)
      res = vecModel.predict([v11, v12])
      err += (cur * BATCH_SIZE)
      i += BATCH_SIZE
      total += BATCH_SIZE
      sys.stderr.write('    %d: %.5f\r' % (i, err / total))
   sys.stderr.write('\n')
   print res
   sys.stderr.write('\n')
   if(total == 0):
      total = 1
   trainError = err / total
   err = 0
   printed = 0
   completed = 0
   ii = 0
   while(ii < TSET_LEN):
      v11 = []
      v12 = []
      v13 = []
      v21 = []
      v22 = []
      v23 = []
      _label = []
      indices = []
      questions = []
      for j in xrange(256):
         if(ii + j >= TSET_LEN):
            break
         idx = testSet[ii + j]
         indices.append(idx)

         s1 = data[idx][0]
         s2 = data[idx][1]

         t1 = data1[idx][0]
         t2 = data1[idx][1]

         questions.append([s1, s2, data1[idx][2]])
         _label.append(data[idx][2])

         v11.append(makeHashVector(t1))
         v12.append(makeGloveVector(s1))

         v21.append(makeHashVector(t2))
         v22.append(makeGloveVector(s2))

      v11 = np.asarray(v11)
      v12 = np.asarray(v12)
      v21 = np.asarray(v21)
      v22 = np.asarray(v22)

      v11 = v11.reshape(v11.shape[0], MAX_CHAR_SEQ_LEN, TRIGRAM_HASHES_LEN, 1)
      v21 = v21.reshape(v21.shape[0], MAX_CHAR_SEQ_LEN, TRIGRAM_HASHES_LEN, 1)
      
      pred = model.predict([v11, v12, v21, v22])

      predLen = len(pred)
      completed += predLen
      assert(len(indices) == predLen)
      assert(len(_label) == predLen)
      for j in xrange(predLen):
         err += logloss(transformLabel(_label[j]), pred[j])
      ii += 256
      if(printed < 6):
         yes = random.randint(0, 200)
         if(yes % 9 == 0):
            print
            printIdx = random.randint(0, predLen - 1)
            print "***************"
            print transformString(questions[printIdx][0])
            print transformString(questions[printIdx][1])
            print transformLabel(_label[printIdx])
            print pred[printIdx]
            assert(_label[printIdx] == questions[printIdx][2])
            print "***************"
            print
            printed += 1
      sys.stderr.write('   processed: %d/%d, %.2f%%\r' % (completed, TSET_LEN, (completed * 100.0) / float(TSET_LEN)))
   print
   print "========================================"
   print "  Epoch      : %d" % (ep)
   print "  Train Error: %.5f" % (trainError)
   print "  Dev Error  : %.5f" % (err / TSET_LEN)
   print "========================================"


from nltk.tokenize import RegexpTokenizer
tokenizer = RegexpTokenizer(r'\w+')

g = open('./predicted.txt', 'w')
g.write('test_id,is_duplicate\n')

TSET_LEN = 2345795

last = None

with open('../test.csv') as f:
   l = 0
   data = csv.reader(f, delimiter = ',')
   idx = 0
   completed = 0
   p11 = []
   p12 = []
   p13 = []
   p21 = []
   p22 = []
   p23 = []
   #next(data)
   questions = []
   for row in data:
      assert(len(row) == 3)
      curId = row[0]
      s1 = row[1].lower()
      s1 = tokenizer.tokenize(s1)

      s2 = row[2].lower()
      s2 = tokenizer.tokenize(s2)

      t1 = ' '.join(s1)
      t2 = ' '.join(s2)

      questions.append([int(row[0]), row[1].lower(), row[2].lower()])

      while(len(t1) > MAX_CHAR_SEQ_LEN):
         t1 = t1[:-1]
      while(len(t1) < MAX_CHAR_SEQ_LEN):
         t1 = ' ' + t1

      while(len(t2) > MAX_CHAR_SEQ_LEN):
         t2 = t2[:-1]
      while(len(t2) < MAX_CHAR_SEQ_LEN):
         t2 = ' ' + t2

      while(len(s1) > MAX_SEQ_LEN):
         s1.pop()
      while(len(s2) > MAX_SEQ_LEN):
         s2.pop()
      while(len(s1) < MAX_SEQ_LEN):
         s1 = ['#'] + s1
      while(len(s2) < MAX_SEQ_LEN):
         s2 = ['#'] + s2

      v11 = makeHashVector(t1)
      v12 = makeGloveVector(s1)

      v21 = makeHashVector(t2)
      v22 = makeGloveVector(s2)

      p11.append(v11)
      p12.append(v12)

      p21.append(v21)
      p22.append(v22)

      if(len(p11) == 512 or l == 2345795):

         last = [int(row[0]), row[1].lower(), row[2].lower()]

         p11 = np.asarray(p11)
         p12 = np.asarray(p12)

         p21 = np.asarray(p21)
         p22 = np.asarray(p22)
      
      
         p11 = p11.reshape(p11.shape[0], MAX_CHAR_SEQ_LEN, TRIGRAM_HASHES_LEN, 1)

         p21 = p21.reshape(p21.shape[0], MAX_CHAR_SEQ_LEN, TRIGRAM_HASHES_LEN, 1)

         pred = model.predict([p11, p12, p21, p22])
         kk = 0
         for kl in pred:
            assert(idx == int(questions[kk][0]))
            g.write('%d,%.5f\n' % (idx, kl[1]))
            #g.write('%d,%.5f,%d,%s,%s\n' % (idx, kl[1], questions[kk][0],questions[kk][1],questions[kk][2]))
            idx += 1
            kk += 1
         #"""
         completed += 512
         p11 = []
         p12 = []
         p13 = []
         p21 = []
         p22 = []
         p23 = []
         questions = []
      l += 1
      if(completed % 1 == 0):
         sys.stderr.write('   processed: %d/%d, %.2f%%\r' % (completed, TSET_LEN, (completed * 100.0) / float(TSET_LEN))) 
print
print l
print idx
print last