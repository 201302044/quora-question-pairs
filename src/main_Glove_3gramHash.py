#!/usr/bin/python

import numpy as np
import os
import sys
import tensorflow as tf
from keras.datasets import mnist
from keras.layers.core import Dense, Dropout, Activation
from keras.layers.embeddings import Embedding
from keras.layers import LSTM, BatchNormalization
from keras.layers import Input, merge
from keras.models import Sequential, Model, load_model
from keras.layers import Dense, Dropout, Input, Lambda, Merge, Flatten, Convolution2D, Convolution1D, MaxPooling2D
from keras.optimizers import SGD, RMSprop, Adam, Nadam, Adadelta
from keras.callbacks import *
from keras.utils import np_utils
from keras import backend as K
from keras import callbacks
import random
import pickle
import gensim
import pandas as pd
from tqdm import tqdm

sess = tf.Session()
K.set_session(sess)

print "Loading Data"
f = open('./trainingData.pkl')
g = open('./developmentData.pkl')
h = open('./trigramHashes.pkl')
e = open('word2tfidf.pkl')

train = pickle.load(f)
dev = pickle.load(g)
hashes = pickle.load(h)
word2tfidf = pickle.load(e)

print len(hashes)

print "initializing Parameters"

BATCHSIZE = 1024
DATALEN = len(train)
DATALENTEST = len(dev)
EPOCHS = 40
DIMENSION = len(hashes)

embeddings_index = {}
f = open('../is_that_a_duplicate_quora_question/data/glove.840B.300d.txt')
for line in tqdm(f):
    values = line.split()
    word = values[0]
    coefs = np.asarray(values[1:], dtype='float32')
    embeddings_index[word] = coefs
f.close()

hsh2idx = {}
j = 0
for i in hashes:
   hsh2idx[i] = j
   j += 1

def makeVector(sent):
   counts = {}
   gloveCumulative = np.zeros(300)
   res = np.zeros(DIMENSION)
   total = 0
   for i in sent:
      try:
         mult = word2tfidf[i]
         gloveCumulative += embeddings_index[i] * mult
         total += 1
      except Exception:
         pass
   for i in sent:
      w = '#' + i + '#'
      wl = len(w)
      for j in xrange(wl):
         if(j + 2 < wl):
            hsh = w[j] + w[j + 1] + w[j + 2]
            res[hsh2idx[hsh]] += 1
   assert(len(res) == DIMENSION)
   if(total == 0):
      total = 1
   return np.asarray(res) / 255.0, gloveCumulative / total

def get(p):
   if(p == 0):
      return [1, 0]
   else:
      assert(p == 1)
      return [0, 1]

def getNext(idx):
   resx1_1 = []
   resx1_2 = []
   resx2_1 = []
   resx2_2 = []
   resy = []
   i = idx
   j = 0
   while(j < BATCHSIZE):
      if(i >= DATALEN):
         break
      else:
         v1, v2 = makeVector(train[i][0])
         resx1_1.append(v1)
         resx1_2.append(v2)
         v1, v2 = makeVector(train[i][1])
         resx2_1.append(v1)
         resx2_2.append(v2)
         resy.append(get(int(train[i][2])))
      j += 1
      i += 1
   assert(len(resx1_1) <= BATCHSIZE)
   assert(len(resx1_2) <= BATCHSIZE)
   assert(len(resy) <= BATCHSIZE)
   return np.asarray(resx1_1), np.asarray(resx1_2), np.asarray(resx2_1), np.asarray(resx2_2), np.asarray(resy)

def getNextTest(idx):
   resx1_1 = []
   resx1_2 = []
   resx2_1 = []
   resx2_2 = []
   resy = []
   i = idx
   j = 0
   while(j < BATCHSIZE):
      if(i >= DATALENTEST):
         break
      else:
         v1, v2 = makeVector(dev[i][0])
         resx1_1.append(v1)
         resx1_2.append(v2)

         v1, v2 = makeVector(dev[i][1])
         resx2_1.append(v1)
         resx2_2.append(v2)
         resy.append(get(int(dev[i][2])))
      j += 1
      i += 1
   assert(len(resx1_1) <= BATCHSIZE)
   assert(len(resx1_2) <= BATCHSIZE)
   assert(len(resy) <= BATCHSIZE)
   return np.asarray(resx1_1), np.asarray(resx1_2), np.asarray(resx2_1), np.asarray(resx2_2), np.asarray(resy)

print "Building model"

input_a1 = Input(shape = (DIMENSION,))
input_a2 = Input(shape = (300,))

input_b1 = Input(shape = (DIMENSION,))
input_b2 = Input(shape = (300,))

sharedNetwork = Sequential()
sharedNetwork.add(Dense(128, activation = 'tanh', input_shape = (DIMENSION,)))
sharedNetwork.add(Dropout(0.2))
sharedNetwork.add(Dense(128, activation = 'tanh'))
sharedNetwork.add(Dropout(0.2))


sharedNetwork1 = Sequential()
sharedNetwork1.add(Dense(128, activation = 'tanh', input_shape = (300,)))
sharedNetwork1.add(Dropout(0.2))
sharedNetwork1.add(Dense(128, activation = 'tanh'))
sharedNetwork1.add(Dropout(0.2))

mergedNetwork = Sequential()
mergedNetwork.add(Merge([sharedNetwork, sharedNetwork1], mode = 'concat'))
mergedNetwork.add(Dense(128, activation = 'tanh'))
mergedNetwork.add(Dropout(0.2))
mergedNetwork.add(Dense(128, activation = 'tanh'))
mergedNetwork.add(Dropout(0.2))

final_1 = mergedNetwork([input_a1, input_a2])
final_2 = mergedNetwork([input_b1, input_b2])
"""

#final_1 = sharedNetwork([input_a1])
#final_2 = sharedNetwork([input_b1])

final_1 = sharedNetwork1([input_a2])
final_2 = sharedNetwork1([input_b2])
"""

predictiveNetwork = Merge(mode = 'concat')([final_1, final_2])
predictiveNetwork = Dense(32, activation = 'tanh')(predictiveNetwork)
predictiveNetwork = Dropout(0.2)(predictiveNetwork)
predictiveNetwork = Dense(32, activation = 'tanh')(predictiveNetwork)
prob = Dense(2, activation = 'softmax')(predictiveNetwork)

print "Compiling model"

model = Model(inputs = [input_a1, input_a2, input_b1, input_b2], outputs = [prob])

model1 = Model(inputs = [input_a1, input_a2], outputs = [final_1])

#optimizer = Adam(lr=0.001)
#optimizer = RMSprop(lr = 0.01)
#optimizer = SGD(lr = 0.1)
optimizer = SGD(lr=0.1, momentum=0.8, nesterov=True, decay=0.004)

model.compile(loss = 'binary_crossentropy', optimizer = optimizer, metrics=['accuracy'])

from keras.utils import plot_model
plot_model(model, to_file='model_gloveFeatures_3gramHashes.png', show_shapes = True)

import math

def getError(y, y_):
   L = len(y)
   assert(L == len(y_))
   res = 0
   for i in xrange(L):
      pred = y[i]
      actual = y_[i]
      res += actual[0] * math.log(pred[0], 10)
      res += actual[1] * math.log(pred[1], 10)
   return -res

for i in xrange(EPOCHS):
   print 'Epoch #%d:' % (i + 1)
   j = 0
   numErr = 0
   numAcc = 0
   den = 0
   vecs = None
   out = None
   while(j < DATALEN):
      x11, x12, x21, x22, y = getNext(j)
      t1, t2 = model.train_on_batch([x11, x12, x21, x22], y)
      vecs = model1.predict([x11, x12])
      out = model.predict([x11, x12, x21, x22])
      numErr += t1
      numAcc += t2
      den += 1
      sys.stderr.write("    %d/%d: %.5f, %.5f\r        " % (j, DATALEN, numErr / float(den), numAcc / float(den)))
      j += BATCHSIZE
   j = 0
   err = 0
   acc = 0
   den = 0
   logloss = 0
   sys.stderr.write('\n')
   #print x12
   #print x22
   j -= BATCHSIZE
   print vecs
   print out[0:0 + 8]
   print y[0:0 + 8]
   for k in xrange(8):
      print train[j + k]
   j = 0
   while(j < DATALENTEST):
      x11, x12, x21, x22, y = getNextTest(j)
      t1, t2 = model.test_on_batch([x11, x12, x21, x22], y)
      err += t1
      acc += t2
      logloss += getError(model.predict([x11, x12, x21, x22]), y)
      vecs = model1.predict([x11, x12])
      out = model.predict([x11, x12, x21, x22])
      den += 1
      sys.stderr.write("    %d/%d: %.5f, %.5f\r        " % (j, DATALENTEST, err / float(den), acc / float(den)))
      #print model.predict([x1, x2])
      j += BATCHSIZE
   print
   #print x12
   #print x22
   print vecs
   print out[-8:]
   print y[-8:]
   print "* Error: %.5f, log loss: %.5f" % (err / float(den), logloss / DATALENTEST)

g = open('./predicted.txt', 'w')
g.write('test_id,is_duplicate\n')

from nltk.tokenize import RegexpTokenizer
tokenizer = RegexpTokenizer(r'\w+')

idx = 0
with open('../test.csv') as f:
    l = 0
    data = csv.reader(f, delimiter = ',')
    for row in data:
        assert(len(row) == 3)
        if(l == 0):
            l += 1
            continue
        l += 1
        if(l % 1000 == 0):
            print l
        q1 = row[1].lower()
        q1 = tokenizer.tokenize(q1)
        q2 = row[2].lower()
        q2 = tokenizer.tokenize(q2)

        v11, v12 = makeVector(q1)
        v21, v22 = makeVector(q2)
        v11 = np.asarray([v11])
        v12 = np.asarray([v12])
        v21 = np.asarray([v21])
        v22 = np.asarray([v22])

        #print model.predict([v1, v2])
        #exit(0)
        g.write(str(idx) + ',' + str(model.predict([v11, v12, v21, v22])[0][1]) + '\n')
        #print net.predict([v1, v2])
        #exit(0)
        #g.write(str(net.predict([v1, v2])[0][0]) + '\n')
        idx += 1

g.close()
         
