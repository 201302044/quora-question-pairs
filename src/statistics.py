import csv
import nltk
from nltk.tokenize import RegexpTokenizer
import random
import sys
import pickle

tokenizer = RegexpTokenizer(r'\w+')

file = open("../test.csv")
L = len(file.readlines())

trigramHashes = set()

def parse(sent):
	for i in sent:
		word = '#' + i + '#'
		wl = len(word)
		for j in xrange(wl):
			if(j + 2 < wl):
				cur = word[j] + word[j + 1] + word[j + 2]
				trigramHashes.add(cur)
a = []

maxL = 0
minL = 10000000
counts = {}

with open('../test.csv') as f:
	l = 0
	data = csv.reader(f, delimiter = ',')
	for row in data:
		q1 = row[1].lower()
		q1 = tokenizer.tokenize(q1)
		q2 = row[2].lower()
		q2 = tokenizer.tokenize(q2)

		l1 = len(q1)
		l2 = len(q2)
		try:
			counts[l1] += 1
		except KeyError:
			counts[l1] = 1

		try:
			counts[l2] += 1
		except KeyError:
			counts[l2] = 1

		maxL = max(maxL, len(q1))
		maxL = max(maxL, len(q2))

		minL = min(minL, len(q1))
		minL = min(minL, len(q2))

		l += 1
		if(l % 100 == 0):
			sys.stderr.write("%d/%d\r      " % (l, L))

for i in counts:
	a.append([counts[i], i])
a.sort(reverse = True)
print "Min:", minL
print "Max:", maxL
print L
agg = 0
for i in a:
	agg += i[0]
	print i[1], i[0], agg